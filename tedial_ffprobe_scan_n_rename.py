# -*- coding: utf-8 -*-

import os  # os.path, os.makedirs, os.listdir, os.name (getting os name), os.remove (DEL file), os.startfile (open file on Windows)
import sys
import subprocess  # excute system commands from python
import re
import time


# adjust the path to your ffprobe location!
ffprobeExePath = "C:\\Users\\simon.dewinter\\Documents\\ffmpeg-4.0.2-win64-static\\bin\\ffprobe.exe"

inputFilePath = raw_input("\n  Gimme a file... \n\n  > ")
inputFilePath = os.path.normpath(inputFilePath.strip(" '").strip(' "'))

inputFileDir = os.path.dirname(inputFilePath)

fileNameInclExt = os.path.basename(inputFilePath)
fileNameWoExt = os.path.splitext(fileNameInclExt)[0]
fileExt = os.path.splitext(fileNameInclExt)[1]

ffprobeCommandLine = '"%s" "%s"' % (ffprobeExePath, inputFilePath)


def selfdestruction():
    raw_input("\n  Press [ENTER] to self-destruct...\n"), time.sleep(0.2)
    print '  Goodbye!\n'
    time.sleep(1), sys.exit()


def runCmdLineAndPutCmdLineOutputAsString(cmdLine):

    # Execute command and get its output...
    cmdLineOutput = subprocess.Popen(cmdLine, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    # Read stdout and catch the output to cmdLineOutputAsString
    cmdLineOutputAsString = ''
    for line in iter(cmdLineOutput.stdout.readline, ''):
        cmdLineOutputAsString += line

    # Our text buffer will end up with \r\r\n, so let's regex replace \r\r for \r
    cmdLineOutputAsString = re.compile(r"\r").sub("", cmdLineOutputAsString)
    return cmdLineOutputAsString


def getNrOfChannelsFromCommandLineOutput(cmdLineOutputAsString):

    nrOfChannels = 0

    for singleLine in re.findall(r'^.*$', cmdLineOutputAsString, re.MULTILINE):

        if bool(re.match(r'.*Stream.*Audio.*(\d channels).*', singleLine)):
            channelsString = re.match(r'.*Stream.*Audio.*(\d channels).*', singleLine).groups(0)[0]
            nrOfChannels += int(channelsString.replace(' channels', ''))

    return nrOfChannels


def constructFileNameSuffix(nrOfChannels):

    if nrOfChannels not in [2, 4, 8]:
        selfdestruction()
    else:
        if nrOfChannels == 2:
            fileNameSuffix = "_nl_20_FullMix"
        if nrOfChannels == 4:
            fileNameSuffix = "_nl-xxYY_20-20_FullMix-MOS"
        if nrOfChannels == 8:
            fileNameSuffix = "_nl-xxYY-xxYY-xxYY_20-20-20-20_FullMix-MOS-MOS-MOS"

    return fileNameSuffix


def constructNewFileName(fileNameWoExt, nrOfChannels):

    fileNameSuffix = constructFileNameSuffix(nrOfChannels)
    newFileName = fileNameWoExt + fileNameSuffix + fileExt

    return newFileName


cmdLineOutputAsString = runCmdLineAndPutCmdLineOutputAsString(ffprobeCommandLine)
nrOfChannels = getNrOfChannelsFromCommandLineOutput(cmdLineOutputAsString)

newFileName = constructNewFileName(fileNameWoExt, nrOfChannels)


# Below is for testing only!
newFilePath = os.path.join(inputFileDir, newFileName)
print newFilePath
